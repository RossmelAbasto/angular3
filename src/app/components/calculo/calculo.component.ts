import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculo',
  templateUrl: './calculo.component.html',
  styleUrls: ['./calculo.component.scss']
})
export class CalculoComponent implements OnInit {
  num1 : number = 4;
  num2 : number = 4;
  suma : number;
  constructor() {
    this.suma = this.num1 + this.num2;  
  }
  
  ngOnInit(): void {
  }
  

}
